"""
Калькулятор на 4 действия: +, -, /, *
Порядок работы:
    1. выбрать операцию введя в строку запроса соответсвующий символ
    2. ввести первый операнд, подтвердить и ввести второй операнд
    3. После подтверждения программа выведет результат вычислений
    4. появится стартовый запрос выбора операции, можно повторить вычисления
    5. для выхода из программы в строку запроса операции достаточно ввести 'q' без кавычек
"""

while True:
    #
    # блок объявления функций   
    #    
    def uinput(): #Ввод операндов
        uinput1 = input("enter first operand: ")
        uinput2 = input("enter second operand: ")
        return uinput1, uinput2

    def summa(x, y): #сложение
        x = int(x)
        y = int(y)
        x += y
        return x

    def minus(x, y): #вычитание
        x = int(x)
        y = int(y)
        x -= y
        return x    

    def devide(x, y): #деление
        x = int(x)
        y = int(y)
        x /= y
        return x 

    def mul(x, y): #деление
        x = int(x)
        y = int(y)
        x *= y
        return x 

    #
    # исполнительный блок
    #
    log = open("log.txt", "w")
    print("Enter operator (+, -, /, *) for action or 'q' for quit")
    user_input = input(": ")

    if user_input == "q":
        break
    elif user_input == "+":
        x, y = uinput()
        last_res = summa(x, y) #хранит последний результат
        print("result: " + str(last_res))
    elif user_input == "-":
        x, y = uinput()
        last_res = minus(x, y) #хранит последний результат
        print("result: " + str(last_res))
    elif user_input == "/":
        x, y = uinput()
        last_res = devide(x, y) #хранит последний результат
        print("result: " + str(last_res))
    elif user_input == "*":
        x, y = uinput()
        last_res = mul(x, y) #хранит последний результат
        print("result: " + str(last_res))
    else:
        print("Unknown input!")